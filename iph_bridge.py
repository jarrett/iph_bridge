#!/usr/bin/env python

from __future__ import print_function
import subprocess
import time

IFCONFIG = '/sbin/ifconfig' 
USBMUXD = '/usr/local/sbin/usbmuxd'
DMESG = '/sbin/dmesg'
client_if = {'ue0'}

def add_bridge_clients(ifconfig, bridge, clients):
    call = [ifconfig, bridge]
    for c in clients:
        call.append('addm')
        call.append(c)
    else:
        call.append('up')
    return call

def if_up(ifconfig, clients):
    for c in clients:
        subprocess.check_call([ifconfig, c, 'up'])

def iphone_wait(timeout):
    print('Please check iPhone for popup messages.', '\nContinuing in...', end='\n')
    for s in range(timeout, 0, -1):
        print('{}...\a'.format(s))
        time.sleep(1)

usbmuxd = subprocess.Popen([USBMUXD, '-U', 'root'])
iphone_wait(3)

client_if.add([x for x in subprocess.check_output([DMESG]).decode().split('\n') if 'ipheth' in x if 'Ethernet' in x][-1][:3])

new_bridge = subprocess.check_output([IFCONFIG, 'bridge', 'create']).strip()
subprocess.check_call(add_bridge_clients(IFCONFIG, new_bridge, client_if))
if_up(IFCONFIG, client_if)
