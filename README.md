# iph_bridge

This script bridges the iPhone or iPad USB ethernet interface to local ethernet interface(s) listed in `client_if` when your iPhone or iPad is plugged in and shuts down your system when the iPhone or iPad is unplugged. This behavior was chosen to allow my headless Raspberry Pi to shut down gracefully when I'm done.

#### Requirements
- FreeBSD (this has only been tested on 10.3)
- Python 2 or 3
- usbmuxd
- `if_ipheth.ko` as a module or built into kernel.
  - Already available as a module if you're running the GENERIC kernel that ships with FreeBSD.

#### How To
1. Clone this repo.
1. Edit `iph_bridge.py` to reflect your local ethernet interface(s).
1. Symlink or copy `iph_bridge.conf` to `/usr/local/etc/devd/iph_bridge.conf`.
  - You will have to create this directory if it doesn't exist.
1. Edit `/usr/local/etc/devd/iph_bridge.conf`'s first action line to reflect the location of `iph_bridge.py`.
1. Restart the devd service.
